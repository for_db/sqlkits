# SQLKits

SQLKits is a Python library that provides utilities for working with SQL databases. 
It offers a simple and consistent interface for creating and managing database connections, executing SQL queries, and handling database operations.

## Features

- **Database Connection Management**: Create and manage connections to SQL databases using SQLAlchemy.
- **Table Operations**: Delete all tables from a database, find datetime columns in ORM classes, and match DataFrames with database tables.
- **Asynchronous Support**: Asynchronous methods for executing database operations.
- Database platform unique support
  - **PostgreSQL Support**: Interact with PostgreSQL databases, including checking superuser status and managing extensions.
  - **TimescaleDB Support**: Work with TimescaleDB databases, including creating and managing hypertables.

## Installation

You can install SQLKits using pip:

```
pip install sqlkits
```

## Usage

Here's a basic example of how to use SQLKits:

```python
from sqlkits import PostgreSQLTool, gen_sqlengine_provider

# Create a SQLEngineProvider
PostgreSQLProvider = gen_sqlengine_provider('postgresql')
db_engine = PostgreSQLProvider(conf={
    'postgresql': {
        'url': {
            'drivername': 'postgresql',
            'host': 'localhost',
            'port': 5432,
            'username': 'your_username',
            'password': 'your_password'
        }
    }
}).provide_sqlengine()

# Create a PostgreSQLTool instance
psql_tool = PostgreSQLTool()

# Check if the current user is a superuser
is_superuser = psql_tool.check_superuser_status(db_engine)
print(f"Is superuser: {is_superuser}")

# Add the PostgreSQL extension
psql_tool.add_extension(db_engine)
```
