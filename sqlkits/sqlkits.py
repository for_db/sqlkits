#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module provides classes and functions for working with SQL databases.
"""

from abc import ABC, abstractmethod
import copy
from enum import Enum
from injector import Module, inject, singleton, provider
from logear import AutoLogException
import operator
import pandas as pd
import re
from represent import ReprHelperMixin
import sqlalchemy
from sqlalchemy import engine as sql_engine, text as sql_text
from sqlalchemy.ext import asyncio as sql_asyncio
from sqlalchemy.orm import as_declarative, declared_attr, declarative_base
from typing import List, Optional, Type


class SQLEngineProvider(ABC, Module):
    """
    Abstract base class for providing SQL engine instances.
    """

    @inject
    def __init__(self, conf: dict):
        """
        Initialize the SQLEngineProvider with the given configuration.

        Args:
            conf (dict): The configuration dictionary.
        """
        self.conf = conf

    @abstractmethod
    def provide_sqlengine(self) -> sql_engine.Engine:
        """
        Abstract method signature for providing an instance of a synchronous SQL engine.

        Returns:
            sql_engine.Engine: The synchronous SQL engine instance.
        """
        raise NotImplementedError()

    @abstractmethod
    def provide_async_sqlengine(self) -> sql_asyncio.AsyncEngine:
        """
        Abstract method signature for providing an instance of an asynchronous SQL engine.

        Returns:
            sql_asyncio.AsyncEngine: The asynchronous SQL engine instance.
        """
        raise NotImplementedError()


def gen_sqlengine_singleton_provider(conf_db_key: str, db_name: Optional[str] = None, ) -> Type[SQLEngineProvider]:
    """
    Generate a SQLEngineProvider class that provides singleton instances of SQL engines.

    Args:
        conf_db_key (str): The key for the database configuration in the configuration dictionary.
        db_name (Optional[str]): The name of the database to use.

    Returns:
        Type[SQLEngineProvider]: The generated SQLEngineProvider class.
    """

    class _SQLEngineProviderImpl(SQLEngineProvider):
        @inject
        def __init__(self, conf: dict):
            super().__init__(conf=conf)
            db_conf = copy.deepcopy(conf.get(conf_db_key, {}))
            if len(db_conf) < 1:
                raise ValueError(
                    "No database config found by {0} key in configuration .json file(s).".format(conf_db_key))
            self.url_conf = db_conf.pop('url', None)
            self.conf = db_conf
            self.db_name = db_name

        @singleton
        @provider
        def provide_sqlengine(self) -> sql_engine.Engine:
            if self.db_name is not None:
                self.url_conf.update({"database": self.db_name})

            sql_url = sql_engine.url.URL.create(**self.url_conf)
            return sql_engine.create_engine(sql_url, **self.conf)

        @singleton
        @provider
        def provide_async_sqlengine(self) -> sql_asyncio.AsyncEngine:
            if self.db_name is not None:
                self.url_conf.update({"database": self.db_name})

            sql_url = sql_engine.url.URL.create(**self.url_conf)
            return sql_asyncio.create_async_engine(sql_url, **self.conf)

    return _SQLEngineProviderImpl


def gen_sqlengine_provider(conf_db_key: str, db_name: Optional[str] = None, ) -> Type[SQLEngineProvider]:
    """
    Generate a SQLEngineProvider class that provides non-singleton instances of SQL engines.

    Args:
        conf_db_key (str): The key for the database configuration in the configuration dictionary.
        db_name (Optional[str]): The name of the database to use.

    Returns:
        Type[SQLEngineProvider]: The generated SQLEngineProvider class.
    """

    class _SQLEngineProviderImpl(SQLEngineProvider):
        @inject
        def __init__(self, conf: dict):
            super().__init__(conf=conf)
            db_conf = copy.deepcopy(conf.get(conf_db_key, {}))
            if len(db_conf) < 1:
                raise ValueError(
                    "No database config found by {0} key in configuration .json file(s).".format(conf_db_key))
            self.url_conf = db_conf.pop('url', None)
            self.conf = db_conf
            self.db_name = db_name

        @provider
        def provide_sqlengine(self) -> sql_engine.Engine:
            if self.db_name is not None:
                self.url_conf.update({"database": self.db_name})

            sql_url = sql_engine.url.URL.create(**self.url_conf)
            return sql_engine.create_engine(sql_url, **self.conf)

        @provider
        def provide_async_sqlengine(self) -> sql_asyncio.AsyncEngine:
            if self.db_name is not None:
                self.url_conf.update({"database": self.db_name})

            sql_url = sql_engine.url.URL.create(**self.url_conf)
            return sql_asyncio.create_async_engine(sql_url, **self.conf)

    return _SQLEngineProviderImpl


class BaseTableMixing(ReprHelperMixin):
    """
    Base class for table mixins.
    """

    @declared_attr
    def __tablename__(cls) -> str:
        """
        Generate the table name from the class name.

        Returns:
            str: The table name.
        """
        return '_'.join(re.findall('[A-Z][^A-Z]*', cls.__name__)).lower()

    def _repr_helper_(self, r):
        """
        Helper method for generating string representations of instances.

        Args:
            r (ReprHelper): The ReprHelper instance.
        """
        for col in sqlalchemy.inspect(self).mapper.column_attrs:
            r.keyword_from_attr(col.key)


class DbMeta(Enum):
    """
    Enum for database metadata.
    """

    def __new__(cls, dialect: str, role_table: str, extension_table: str, extension: str, params_limit: int):
        """
        Create a new instance of the DbMeta enum.

        Args:
            dialect (str): The database dialect.
            role_table (str): The name of the role table.
            extension_table (str): The name of the extension table.
            extension (str): The name of the database extension.
            params_limit (int): The limit for the number of parameters.

        Returns:
            DbMeta: The new instance of the DbMeta enum.
        """

        obj = object.__new__(cls)
        obj.dialect = dialect
        obj.role_table = role_table
        obj.extension_table = extension_table
        obj.extension = extension
        obj.params_limit = params_limit
        return obj

    PostgreSQL = 'postgresql', 'pg_roles', 'pg_extension', 'plpgsql', 65535
    TimescaleDB = 'postgresql', 'pg_roles', 'pg_extension', 'timescaledb', 65535


@AutoLogException.adapt_autolog_exception()
class BaseSQLTool(ABC):
    """
    Abstract base class for SQL tools.
    """

    @property
    @abstractmethod
    def db_meta(self) -> DbMeta:
        """
        Abstract signature method to get the database metadata.

        Returns:
            DbMeta: The database metadata.
        """

        raise NotImplementedError()

    def add_extension(self, db_engine: sql_engine.Engine):
        """
        Add a database extension to the given SQL engine.

        Args:
            db_engine (sql_engine.Engine): The SQL engine instance.
        """

        create_extension_sql_text = sql_text(
            'CREATE EXTENSION IF NOT EXISTS "{0}" CASCADE;'.format(self.db_meta.extension))

        with db_engine.connect() as conn:
            conn.execute(create_extension_sql_text)
            conn.commit()

    def delete_extension(self, db_engine: sql_engine.Engine):
        """
        Delete a database extension from the given SQL engine.

        Args:
            db_engine (sql_engine.Engine): The SQL engine instance.
        """

        drop_extension_sql_text = sql_text(
            'DROP EXTENSION IF EXISTS "{0}" CASCADE;'.format(self.db_meta.extension))

        with db_engine.connect() as conn:
            conn.execute(drop_extension_sql_text)
            conn.commit()

    # TODO: Create async methods

    def delete_all_tables(self, db_engine: sql_engine.Engine):
        """
        Delete all tables from the given SQL engine.

        Args:
            db_engine (sql_engine.Engine): The SQL engine instance.
        """

        with db_engine.connect() as conn:
            for table in sqlalchemy.inspect(subject=db_engine).get_table_names():
                drop_table_sql_text = sql_text('DROP TABLE IF EXISTS "{0}" CASCADE'.format(table))
                conn.execute(drop_table_sql_text)
            conn.commit()

    async def a_delete_all_tables(self, db_engine: sql_asyncio.AsyncEngine):
        """
        Asynchronously delete all tables from the given SQL engine.

        Args:
            db_engine (sql_asyncio.AsyncEngine): The asynchronous SQL engine instance.
        """

        async with db_engine.connect() as conn:
            for table in sqlalchemy.inspect(subject=db_engine).get_table_names():
                drop_table_sql_text = sql_text('DROP TABLE IF EXISTS "{0}" CASCADE'.format(table))
                await conn.execute(drop_table_sql_text)
            await conn.commit()

    def find_datetime_columns(self, orm_class: type) -> List[str]:
        """
        Find the datetime columns in the given ORM class.

        Args:
            orm_class (Type): The ORM class.

        Returns:
            List[str]: A list of datetime column names.
        """

        inspector = sqlalchemy.inspect(orm_class)

        columns = []
        for column in inspector.mapper.columns.values():
            if isinstance(column.type, sqlalchemy.DateTime):
                columns.append(column.name)
        return columns

    def get_table_columns(self, table_name: str, db_engine: sql_engine.Engine) -> set[str]:
        """
        Get the columns of the given table from the SQL engine.

        Args:
            table_name (str): The name of the table.
            db_engine (sql_engine.Engine): The SQL engine instance.

        Returns:
            set[str]: A set of column names.
        """

        # This method does not include relation field in output.
        sql_inspector = sqlalchemy.inspect(db_engine)
        columns_info = sql_inspector.get_columns(table_name)
        return set(map(lambda c: c['name'], columns_info))

    def match_asset_df(self,
                       df: pd.DataFrame,
                       table_name: str,
                       exist: bool,
                       db_engine: sql_engine.Engine) -> pd.DataFrame | None:
        """
        Match the given DataFrame with the table in the SQL engine based on the existence condition.

        Args:
            df (pd.DataFrame): The input DataFrame.
            table_name (str): The name of the table.
            exist (bool): Whether to match rows that exist or do not exist in the table.
            db_engine (sql_engine.Engine): The SQL engine instance.

        Returns:
            pd.DataFrame or None: The matched DataFrame or None if no rows match.
        """

        # This method does not include relation field in output.

        df_column_set = set(df.columns.to_list())
        asset_table_columns = self.get_table_columns(table_name=table_name, db_engine=db_engine)
        if len(asset_table_columns) < len(df_column_set):
            raise ValueError("{0} table columns {1} is less than {0} dataframe columns {2}.".format(
                table_name, asset_table_columns, df_column_set))

        column_xor = asset_table_columns ^ df_column_set
        asset_table_column_only = asset_table_columns - df_column_set
        df_column_only = column_xor - asset_table_column_only
        if len(df_column_only) > 0:
            raise ValueError("{0} dataframe columns has column(s) not found in {0} table columns: {1}.".format(
                table_name, df_column_only))

        Base = declarative_base()
        Base.metadata.reflect(bind=db_engine)
        table = Base.metadata.tables[table_name]

        result_rows = []
        gen_session = sqlalchemy.orm.sessionmaker(bind=db_engine)
        session = gen_session()
        for i in range(0, df.shape[0]):
            row = df.iloc[i, :]
            row_dict = row.to_dict()

            query_filter = []
            for c in df_column_set:
                attr_getter = operator.attrgetter(c)
                query_filter.append(attr_getter(table.columns) == row_dict[c])

            query_out = session.query(table).filter(*query_filter).all()
            if exist and len(query_out) > 0:
                result_rows.append(row)
            elif not exist and len(query_out) < 1:
                result_rows.append(row)

        return pd.DataFrame(result_rows) if len(result_rows) > 0 else None

    def delete_all_entry(self, table_name: str, db_engine: sql_engine.Engine):
        """
        Delete all entries from the given table in the SQL engine.

        Args:
            table_name (str): The name of the table.
            db_engine (sql_engine.Engine): The SQL engine instance.
        """

        gen_session = sqlalchemy.orm.sessionmaker(bind=db_engine)
        session = gen_session()

        Base = declarative_base()
        Base.metadata.reflect(bind=db_engine)
        table = Base.metadata.tables[table_name]

        session.query(table).delete()
        session.commit()
        session.close()


class PostgreSQLTool(BaseSQLTool):
    """
    A tool for working with PostgreSQL databases.
    """

    @property
    def db_meta(self) -> DbMeta:
        """
        Get the database metadata for PostgreSQL.

        Returns:
            DbMeta: The database metadata.
        """

        return DbMeta.PostgreSQL

    def is_postgresql(self, db_engine) -> bool:
        """
        Check if the given SQL engine is a PostgreSQL engine.

        Args:
            db_engine (sql_engine.Engine): The SQL engine instance.

        Returns:
            bool: True if the engine is a PostgreSQL engine, False otherwise.
        """

        if db_engine.dialect.name.lower() != DbMeta.PostgreSQL.dialect:
            return False

    def check_superuser_status(self, db_engine: sql_engine.Engine) -> bool:
        """
        Check if the current user is a superuser in the given SQL engine.

        Args:
            db_engine (sql_engine.Engine): The SQL engine instance.

        Returns:
            bool: True if the current user is a superuser, False otherwise.
        """

        query_sql_text = sql_text(
            "SELECT rolsuper FROM {0} WHERE rolname = CURRENT_USER;".format(self.db_meta.role_table))

        with db_engine.connect() as conn:
            result = conn.execute(query_sql_text)
            row = result.fetchone()
            is_superuser = row[0]
            return is_superuser


class TimescaleDbTool(PostgreSQLTool):
    """
    A tool for working with TimescaleDB databases.
    """

    @property
    def db_meta(self) -> DbMeta:
        """
        Get the database metadata for TimescaleDB.

        Returns:
            DbMeta: The database metadata.
        """

        return DbMeta.TimescaleDB

    @staticmethod
    def is_timescaledb(db_engine: sql_engine.Engine) -> bool:
        """
        Check if the given SQL engine is a TimescaleDB engine.

        Args:
            db_engine (sql_engine.Engine): The SQL engine instance.

        Returns:
            bool: True if the engine is a TimescaleDB engine, False otherwise.
        """

        if db_engine.dialect.name.lower() != DbMeta.TimescaleDB.dialect:
            return False

        is_timescaledb = False
        for schema in sqlalchemy.inspect(subject=db_engine).get_schema_names():
            if re.match('^timescaledb', schema):
                is_timescaledb = True
                break
        if not is_timescaledb:
            return False

        return True

    # TODO: Rename to to_hypertable
    def to_hypertable(self,
                      db_engine: sql_engine.Engine,
                      table_name: str,
                      time_column_name: str):
        """
        Create a hypertable in the given TimescaleDB database.
        This function only supports TimescaleDB.
        Args:
            db_engine (sql_engine.Engine): The SQL engine instance.
            table_name (str): The name of the table.
            time_column_name (str): The name of the time type column.
        """

        if not self.is_timescaledb(db_engine):
            raise TypeError('create_hypertable function is only supported for TimescaleDB.')

        if not re.match(r'^\w+$', table_name):
            raise ValueError("Invalid table_name value: {0}.".format(table_name))
        if not re.match(r'^\w+$', time_column_name):
            raise ValueError("Invalid time_column_name value: {0}.".format(time_column_name))

        create_hypertable_sql_text = sql_text('SELECT create_hypertable(:table_name, :time_column_name);')

        with db_engine.connect() as conn:
            conn.execute(create_hypertable_sql_text,
                         parameters={'table_name': table_name, 'time_column_name': time_column_name})
            conn.commit()

    def is_hypertable(self, db_engine: sql_engine.Engine, table_name: str, schema_name='public') -> bool:
        """
        Check if the given table is a hypertable in the given TimescaleDB database.
        This function only supports TimescaleDB.

        Args:
            db_engine (sql_engine.Engine): The SQL engine instance.
            table_name (str): The name of the table.
            schema_name (str): The name of the schema.
        Returns:
            bool: True if the table is a hypertable, False otherwise.
        """
        if not self.is_timescaledb(db_engine=db_engine):
            raise ValueError("{0} is not a TimescaleDB database.".format(db_engine.url.database))

        inspector = sqlalchemy.inspect(db_engine)

        # Check for table existence and chunk_id column
        if table_name not in inspector.get_table_names():
            raise ValueError("Not found {0} table.".format(table_name))

        if schema_name not in inspector.get_schema_names():
            raise ValueError("Not found {0} schema.".format(schema_name))

        with db_engine.connect() as connection:
            result = connection.execute(sql_text("""
                SELECT EXISTS (
                    SELECT status
                    FROM "_timescaledb_catalog".hypertable
                    WHERE schema_name = :schema_name AND table_name = :table_name
                );
            """), {'table_name': table_name, 'schema_name': schema_name})

            return result.scalar()
