#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import atexit

from confijector.ConfigLoader import config_injector_factory
import functools
import glob
from icecream import ic
from injector import Injector
import logging
import os
import pickle
import pytest

from sqlkits.sqlkits import *
from tests.orm.TradeAssetDomain import *

import sqlalchemy


@pytest.fixture(scope='module')
def test_conf_file_fixture() -> str:
    return os.path.join(os.path.curdir, '.config.test.json')


@pytest.fixture(scope='module', autouse=True)
def config_injector_fixture(test_conf_file_fixture: str) -> Injector:
    ic.disable()
    injector = config_injector_factory(conf_files=[test_conf_file_fixture])
    ic.enable()
    return injector


@pytest.fixture(scope='module', autouse=True)
def timescaledbtool_fixture() -> TimescaleDbTool:
    return TimescaleDbTool()


class DbEngineContainer:
    _conf_files = []
    _db_engine = None

    @classmethod
    def set_conf_files(cls, conf_files: List[str]):
        cls._conf_files = conf_files

    @classmethod
    def get_db_engine(cls, recreate: bool = False) -> sql_engine.Engine:
        if cls._conf_files is None or len(cls._conf_files) < 1:
            raise RuntimeError("configuration files need to be preset.")

        if cls._db_engine is None or recreate:
            ic.disable()
            injector = config_injector_factory(conf_files=cls._conf_files)
            injector = injector.create_child_injector(
                injector.get(gen_sqlengine_provider(conf_db_key='timescaledb')))
            cls._db_engine = cast(sql_engine.Engine, injector.get(sql_engine.Engine))
            ic.enable()

            atexit.register(functools.partial(
                lambda foo: print(foo),
                "cls._db_engine.url = {0}".format(cls._db_engine.url)
            ))

        return cls._db_engine


@pytest.fixture(scope='module', autouse=True)
def db_engine_fixture(test_conf_file_fixture: str) -> sql_engine.Engine:
    DbEngineContainer.set_conf_files(conf_files=[test_conf_file_fixture])
    return DbEngineContainer.get_db_engine()


def reset_db_engine_fixture() -> sql_engine.Engine:
    return DbEngineContainer.get_db_engine(recreate=True)


@pytest.fixture(scope='module', autouse=True)
def asset_df_fixture() -> pd.DataFrame:
    asset_df_pkl_path = list(glob.iglob(os.path.sep.join(['tests', '*', 'asset_df.pkl']), recursive=True))[0]
    return pickle.load(open(asset_df_pkl_path, 'rb'))


@AutoLogException.adapt_autolog_exception()
class BaseTest(ABC):
    @pytest.fixture(scope='class', autouse=True)
    def logger_fixture(self):
        logger = cast(logging.Logger, self.grab_logger())
        ic.configureOutput(includeContext=True,
                           outputFunction=(lambda s: logger.debug(s)))
        self.logger = logger
        return logger


class TestTimescaleDbTool(BaseTest):
    def test_is_timescaledb(self, timescaledbtool_fixture: TimescaleDbTool, db_engine_fixture: sql_engine.Engine):
        assert timescaledbtool_fixture.is_timescaledb(db_engine_fixture), \
            "Expected to detect TimescaleDB as connected database server, but actually not"

    def test_check_superuser_status(
            self, caplog, timescaledbtool_fixture: TimescaleDbTool, db_engine_fixture: sql_engine.Engine):
        ic.enable()
        caplog.set_level(logging.DEBUG)

        assert timescaledbtool_fixture.check_superuser_status(db_engine=db_engine_fixture), \
            "Expected super user privilege with current database user account but actual not."

    def test_delete_extension(self,
                              timescaledbtool_fixture: TimescaleDbTool,
                              db_engine_fixture: sql_engine.Engine):
        extension_query_sql_text = sql_text("SELECT * FROM pg_extension WHERE extname = :extension;")

        timescaledbtool_fixture.delete_extension(db_engine=db_engine_fixture)
        with db_engine_fixture.connect() as conn:
            cursor = conn.execute(extension_query_sql_text,
                                  parameters={'extension': timescaledbtool_fixture.db_meta.extension})
            assert cursor.rowcount < 1, \
                "Expected not to detect timescaledb extension after deletion, but actually detected."
        db_engine_fixture.clear_compiled_cache()
        db_engine_fixture.dispose()

        db_engine_fixture = reset_db_engine_fixture()
        timescaledbtool_fixture.add_extension(db_engine=db_engine_fixture)
        with db_engine_fixture.connect() as conn:
            cursor = conn.execute(extension_query_sql_text,
                                  parameters={'extension': timescaledbtool_fixture.db_meta.extension})
            assert cursor.rowcount > 0, "Expected for timescaledb extension added, but actually not."

    def setup_tables(self, db_engine: sql_engine.Engine):
        create_asset_table_sql_text = sqlalchemy.text("""
        CREATE TABLE if not exists public.asset (
                id SERIAL NOT NULL,
                symbol VARCHAR NOT NULL,
                name VARCHAR NOT NULL,
                exchange VARCHAR NOT NULL,
                type VARCHAR NOT NULL,
                is_etf BOOLEAN,
                status VARCHAR NOT NULL,
                last_update TIMESTAMP WITH TIME ZONE NOT NULL,
                PRIMARY KEY (id),
                CONSTRAINT type_exchange_symbol_key UNIQUE (type, exchange, symbol)
        );        
        """)

        create_asset_price_table_sql_text = sqlalchemy.text("""
        CREATE TABLE if not exists public.asset_price (
                asset_id INTEGER NOT NULL,
                dt TIMESTAMP WITHOUT TIME ZONE NOT NULL,
                open NUMERIC NOT NULL,
                close NUMERIC NOT NULL,
                adj_close NUMERIC,
                high NUMERIC NOT NULL,
                low NUMERIC NOT NULL,
                volume NUMERIC NOT NULL,
                PRIMARY KEY (asset_id, dt),
                FOREIGN KEY(asset_id) REFERENCES asset (id)
        );
        CREATE INDEX if not exists idx_asset_id_dt ON asset_price (asset_id, dt DESC);
        """)

        with db_engine.connect() as conn:
            conn.execute(create_asset_table_sql_text)
            conn.execute(create_asset_price_table_sql_text)
            conn.commit()

    def test_delete_all_tables(
            self, caplog, timescaledbtool_fixture: TimescaleDbTool, db_engine_fixture: sql_engine.Engine):
        self.setup_tables(db_engine=db_engine_fixture)

        tables = sqlalchemy.inspect(subject=db_engine_fixture).get_table_names()
        assert len(tables) > 0, "TEST ERROR: none table available for testing."

        timescaledbtool_fixture.delete_all_tables(db_engine=db_engine_fixture)
        tables = sqlalchemy.inspect(subject=db_engine_fixture).get_table_names()
        assert len(tables) < 1, "Expected for all tables deleted, but actually not: {0}".format(tables)

    def get_table_name(self, orm_cls: type):
        return '_'.join(re.findall('[A-Z][^A-Z]*', orm_cls.__name__)).lower()

    def test_create_all_tables(self, timescaledbtool_fixture: TimescaleDbTool, db_engine_fixture: sql_engine.Engine):
        timescaledbtool_fixture.delete_all_tables(db_engine=db_engine_fixture)
        tables = sqlalchemy.inspect(subject=db_engine_fixture).get_table_names()
        assert len(tables) < 1, \
            "TEST ERROR: Expected none table as test environment, but actual detected tables: {0}.".format(tables)

        create_all_tables(db_engine=db_engine_fixture)

        tables = sqlalchemy.inspect(subject=db_engine_fixture).get_table_names()
        assert len(tables) == 3, "Expected 3 tables created, but actual not: {0}".format(tables)

        expected_tables = {self.get_table_name(orm_cls=Asset),
                           self.get_table_name(orm_cls=EtfHolding),
                           self.get_table_name(orm_cls=AssetPrice)}
        assert expected_tables == set(tables), \
            "Expected tables {0}, but actual {1}.".format(expected_tables, set(tables))

        # TODO: confirm about index on asset_price table
        # TODO: confirm about constraint on asset table

    def test_to_hypertable(self, timescaledbtool_fixture: TimescaleDbTool, db_engine_fixture: sql_engine.Engine):
        self.setup_tables(db_engine=db_engine_fixture)

        table_name = self.get_table_name(orm_cls=AssetPrice)
        columns = timescaledbtool_fixture.find_datetime_columns(orm_class=AssetPrice)
        assert len(columns) == 1, \
            "TEST ERROR: Expected count of DataTime column is one in {0} table but actual {1}.".format(
                table_name, columns)
        timescaledbtool_fixture.to_hypertable(db_engine=db_engine_fixture,
                                              table_name=table_name,
                                              time_column_name=columns[0])

        assert timescaledbtool_fixture.is_hypertable(db_engine=db_engine_fixture, table_name=table_name)

    def test_is_hypertable(self, timescaledbtool_fixture: TimescaleDbTool, db_engine_fixture: sql_engine.Engine):
        self.setup_tables(db_engine=db_engine_fixture)

        table_name = self.get_table_name(orm_cls=Asset)
        assert not timescaledbtool_fixture.is_hypertable(db_engine=db_engine_fixture, table_name=table_name)

        table_name = self.get_table_name(orm_cls=AssetPrice)
        assert timescaledbtool_fixture.is_hypertable(db_engine=db_engine_fixture, table_name=table_name)

    def test_match_asset_df(self,
                            timescaledbtool_fixture: TimescaleDbTool,
                            db_engine_fixture: sql_engine.Engine,
                            asset_df_fixture: pd.DataFrame):
        self.setup_tables(db_engine=db_engine_fixture)

        added_rows = asset_df_fixture.to_sql(cast(sqlalchemy.Table, Asset.__table__).name,
                                             con=db_engine_fixture,
                                             index=False,
                                             if_exists='append')

        matched_asset_df = timescaledbtool_fixture.match_asset_df(
            df=asset_df_fixture,
            table_name=cast(sqlalchemy.Table, Asset.__table__).name,
            exist=True,
            db_engine=db_engine_fixture)
        reindex_columns = ['type', 'exchange', 'symbol', 'last_update']
        matched_asset_df = matched_asset_df.reindex(columns=reindex_columns)
        asset_df_fixture = asset_df_fixture.reindex(columns=reindex_columns)
        assert asset_df_fixture.equals(matched_asset_df), \
            ("Expected all asset data had added to {0} table from pandas' DataFarme would have located in {0} table, "
             "but actual {1}").format(asset_df_fixture.compare(matched_asset_df))
