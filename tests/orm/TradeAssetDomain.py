import asyncio
from datetime import datetime
import sqlalchemy
from sqlalchemy.ext import asyncio as sql_asyncio
from sqlalchemy.sql import schema as sql_schema
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, MappedAsDataclass, relationship
from sqlkits.sqlkits import BaseTableMixing
from typing import cast, List, Set


class BaseTable(DeclarativeBase, MappedAsDataclass, BaseTableMixing):
    pass


class AssetPrice(BaseTable):
    asset_id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey('asset.id'), nullable=False, primary_key=True)
    dt: Mapped[datetime] = mapped_column(sqlalchemy.DateTime, nullable=False, primary_key=True)
    open: Mapped[sqlalchemy.Numeric] = mapped_column(sqlalchemy.Numeric, nullable=False)
    close: Mapped[sqlalchemy.Numeric] = mapped_column(sqlalchemy.Numeric, nullable=False)
    adj_close: Mapped[sqlalchemy.Numeric] = mapped_column(sqlalchemy.Numeric, nullable=True)
    high: Mapped[sqlalchemy.Numeric] = mapped_column(sqlalchemy.Numeric, nullable=False)
    low: Mapped[sqlalchemy.Numeric] = mapped_column(sqlalchemy.Numeric, nullable=False)
    volume: Mapped[sqlalchemy.Numeric] = mapped_column(sqlalchemy.Numeric, nullable=False)

    @classmethod
    def set_index(cls) -> Set[str]:
        # Attention: This cannot set index to already-existing table in database server.
        table = cls.metadata.tables[cls.__tablename__]

        index_name = 'idx_{0}_{1}'.format(cls.asset_id.name, cls.dt.name)
        indexes = set()
        if hasattr(table, '__table_args__'):
            for i in range(0, len(table.__table_args__)):
                if not isinstance(table.__table_args__[i], sql_schema.Index):
                    continue
                indexes.update({cast(sql_schema.Index, table.__table_args__[i]).name})

            if index_name not in indexes:
                cast(sqlalchemy.Table, table).append_constraint(
                    constraint=sql_schema.Index(index_name, cls.asset_id.name, cls.dt.desc()))
                indexes.update({index_name})
        else:
            cast(sqlalchemy.Table, table).append_constraint(
                constraint=sql_schema.Index(index_name, cls.asset_id.name, cls.dt.desc()))
            indexes.update({index_name})

        return indexes


class Asset(BaseTable):
    id: Mapped[int] = mapped_column(sqlalchemy.Integer, init=False, primary_key=True)
    symbol: Mapped[str] = mapped_column(sqlalchemy.String, nullable=False)
    name:  Mapped[str] = mapped_column(sqlalchemy.String, nullable=False)
    exchange: Mapped[str] = mapped_column(sqlalchemy.String, nullable=False)
    type: Mapped[str] = mapped_column(sqlalchemy.String, nullable=False)
    is_etf: Mapped[bool] = mapped_column(sqlalchemy.Boolean, nullable=True)
    status: Mapped[str] = mapped_column(sqlalchemy.String, nullable=False)
    last_update: Mapped[datetime] = mapped_column(sqlalchemy.DateTime(timezone=True),
                                                  nullable=False,
                                                  default=sqlalchemy.func.now(),
                                                  insert_default=sqlalchemy.func.now())

    asset_price: Mapped[List[AssetPrice]] = relationship(
        'AssetPrice', init=False, cascade='all, delete-orphan')

    @classmethod
    def set_constraint(cls) -> Set[str]:
        # Attention: This cannot set constraint to already-existing table in database server.

        table = cls.metadata.tables[cls.__tablename__]

        constraint_name = "{0}_{1}_{2}_key".format(cls.type.name, cls.exchange.name, cls.symbol.name)
        constraints = set()
        if hasattr(cls, '__table_args__'):
            for i in range(0, len(table.__table_args__)):
                if not isinstance(table.__table_args__[i], sqlalchemy.UniqueConstraint):
                    continue
                constraints.update({str(cast(sqlalchemy.UniqueConstraint, table.__table_args__[i]).name)})

            if constraint_name not in constraints:
                cast(sqlalchemy.Table, table).append_constraint(
                    constraint=sqlalchemy.UniqueConstraint(cls.type.name,
                                                           cls.exchange.name,
                                                           cls.symbol.name,
                                                           name=constraint_name))
                constraints.update({constraint_name})
        else:
            cast(sqlalchemy.Table, table).append_constraint(
                constraint=sqlalchemy.UniqueConstraint(cls.type.name,
                                                       cls.exchange.name,
                                                       cls.symbol.name,
                                                       name=constraint_name))
            constraints.update({constraint_name})
        return constraints


class EtfHolding(BaseTable):
    etf_id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey('asset.id'), nullable=False, primary_key=True)
    holding_id: Mapped[int] = mapped_column(
        sqlalchemy.Integer, sqlalchemy.ForeignKey('asset.id'), nullable=False, primary_key=True)
    quantity: Mapped[sqlalchemy.Numeric] = mapped_column(sqlalchemy.Numeric)
    weight: Mapped[sqlalchemy.Numeric] = mapped_column(sqlalchemy.Numeric)
    dt: Mapped[datetime] = mapped_column(sqlalchemy.DateTime,
                                         nullable=False,
                                         primary_key=True,
                                         default=sqlalchemy.func.now(),
                                         server_default=sqlalchemy.func.now())


def create_all_tables(db_engine: sqlalchemy.Engine):
    AssetPrice.set_index()
    Asset.set_constraint()
    BaseTable.metadata.create_all(bind=db_engine)


async def async_create_all_tables(async_db_engine: sql_asyncio.AsyncEngine):
    AssetPrice.set_index()
    Asset.set_constraint()
    async with async_db_engine.begin() as conn:
        await conn.run_sync(BaseTable.metadata.create_all)
